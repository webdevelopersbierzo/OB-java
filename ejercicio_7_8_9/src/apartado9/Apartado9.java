package apartado9;

import java.io.*;
import java.util.NoSuchElementException;
import java.util.Scanner;

/*
        *Utilizando InputStream y PrintStream, crea una función que reciba dos parámetros: "fileIn" y "fileOut".
        *  La tarea de la función será realizar la copia del fichero dado en el parámetro "fileIn" al fichero
        * dado en "fileOut".
        */
public class Apartado9 {
    public static void main(String[] args) {

        String url1;
        String url2;

        url1 = getpath();
        url2 = getpath();
        copyFile(url1,url2);


    }

    /**
     * Function that asks us for the file path
     * @return String path to file
     */
    public static String getpath(){
        String path="";
        boolean ok = false;
        do{
            Scanner sc = new Scanner(System.in);
            System.out.println("Introduce el la ruta del archivo : ");
            sc.reset();
            try{
                path = sc.nextLine();
                ok = true;
            }catch(NoSuchElementException e){
                System.out.println("No has escrito nada" + e.getMessage());
            }catch(IllegalStateException e){
                System.out.println("El sd Scanner esta cerrado" + e.getMessage());
            }

        }while(!ok);

        return path;
    }

    /**
     * Function that copies a file to another file
     * @param fileIn input file
     * @param fileOut output file
     */
    public static void copyFile(String fileIn, String fileOut){

        try{
            InputStream in = new FileInputStream(fileIn);
            BufferedInputStream inBuffer = new BufferedInputStream(in);
            try{
                int dato = inBuffer.read();
                PrintStream out = new PrintStream(fileOut);
                while(dato != -1){

                    if(dato != -1){
                        out.write(dato);
                    }
                    dato = inBuffer.read();

                }
            }catch(IOException e ){
                System.out.println("No se ha podido  leer el fichero; " + e.getMessage());
            }
        }catch (FileNotFoundException e){
            System.out.println("Oye que el programa da error " + e.getMessage());
        }

    }

}