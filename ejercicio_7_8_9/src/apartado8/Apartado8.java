package apartado8;

import java.util.InputMismatchException;
import java.util.Scanner;

/*
 *Crea una función DividePorCero.
 *  Esta, debe generar una excepción ("throws") a su llamante del tipo ArithmeticException
 *   que será capturada por su llamante (desde "main", por ejemplo).
 * Si se dispara la excepción, mostraremos el mensaje "Esto no puede hacerse".
 *  Finalmente, mostraremos en cualquier caso: "Demo de código".
 */
public class Apartado8 {
    public static void main(String[] args) {

        boolean ok = false; // flag
        do {
            Scanner sc = new Scanner(System.in);
            System.out.println("Introduce dos numeros");
            sc.reset();
            try{ // capturamos la excepcion de entrada
                int a = sc.nextInt();
                int b = sc.nextInt();
                ok = true;
                System.out.println(dividePorCero(a,b));
            }catch (InputMismatchException e){
                System.out.println("Has introducido valores invalidos");
            }

        } while (!ok);

    }

    public static double dividePorCero(int numero, int numero2){
        double resultado = 0;

        try{ // capturamos excepcion
            resultado =(double)numero/numero2; // cast para convertirlo a double

        }catch (ArithmeticException e){
            System.out.println("Esto no  puede hacerse has intentanto dividir entre " + numero2 );

        }
        System.out.println("Demo del codigo: \n El resultado es : ");
        return resultado;






    }
}
