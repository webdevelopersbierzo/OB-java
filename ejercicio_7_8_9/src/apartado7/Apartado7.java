package apartado7;

import java.util.ArrayList;

/*
 *Crea un ArrayList de tipo int, y, utilizando un bucle rellénalo con elementos 1..10.
 *  A continuación, con otro bucle, recórrelo y elimina los numeros pares.
 *  Por último, vuelve a recorrerlo y muestra el ArrayList final. Si te atreves,
 *  puedes hacerlo en menos pasos, siempre y cuando cumplas el primer "for" de relleno.
 */
public class Apartado7 {
    public static void main(String[] args) {
        // creamos el arrayList

        ArrayList<Integer> numeros = new ArrayList<Integer>(9);
        for(int i = 0; i < 10; i++){
            numeros.add(i,(i + 1));
            System.out.print(" " + numeros.get(i));
        }

        // Primera solucion
        System.out.println("\n Primera solucion");
        for(int i = 0; i < numeros.size(); i++){
            if(numeros.get(i)%2 != 0){
                numeros.remove(i);
            }
            System.out.print(" " + numeros.get(i));
        }

        // segunda solución

        System.out.println("\n Segunda solucion: ");
        for(int i = 0; i < 10; i++){
            numeros.add(i, (i + 1));
            if(numeros.get(i)%2 !=0){
                numeros.remove(i);

            }else{
                System.out.print(" " + numeros.get(i));
            }

        }



    }
}
