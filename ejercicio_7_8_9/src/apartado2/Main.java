package apartado2;

/*
 * Crea un array unidimension de Strin y recorrelo mostrando unicamente sus valores
 */
public class Main {
    public static void main(String[] args) {
        String[] arrayNombres = {
                "Oscar Gracia de  Garcia",
                "Juan",
                "Gonzalo"
        };
        for(String nombre: arrayNombres){
            System.out.println("Nombre " + nombre);
        }
    }


}