package apartado5;

import java.util.Vector;

public class Apartado5 {
    public static void main(String[] args) {
        System.out.println("el problema es que el vector por defecto nos crea un array de 10 elementos");
        Vector<Integer> numeros = new Vector<Integer>();
        System.out.println("la capacidad de vector numeros es " + numeros.capacity());
        System.out.println("tendriamos que ir añadiendo uno a uno los elementos asi hasta mil, no es eficiente");
    }
}
