package apartado3;

/*
 * crea un array bidimensional de enteros y recorrelo, mostrando unicamente sus valores
 */
public class Apartado3 {
    public static void main(String[] args) {
        int numerosBi[][]= new int[3][4];
        numerosBi[0][0] = 0;
        numerosBi[0][1] = 1;
        numerosBi[0][2] = 3;
        numerosBi[0][3] = 4;

        numerosBi[1][0] = 10;
        numerosBi[1][1] = 11;
        numerosBi[1][2] = 12;
        numerosBi[1][3] = 13;

        numerosBi[2][0] = 20;
        numerosBi[2][1] = 21;
        numerosBi[2][2] = 22;
        numerosBi[2][3] = 23;


        System.out.println("Imprimiendo el vector: ");

        for (int i = 0; i <numerosBi.length; i++){


            for (int j = 0; j < numerosBi[i].length; j++){
                System.out.println("[" + i + "] " + "[" + j +"] " + numerosBi[i][j]);

            }

        }
    }
}
