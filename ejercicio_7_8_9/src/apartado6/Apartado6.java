package apartado6;

/*
        *Crea un ArrayList de tipo String, con 4 elementos. Cópialo en una LinkedList.
        * Recorre ambos mostrando únicamente el valor de cada elemento.
        */
import java.util.ArrayList;
import java.util.LinkedList;

public class Apartado6 {
    public static void main(String[] args) {
        ArrayList<String> cadenas = new ArrayList<String>(4);
        cadenas.add("pepe");
        cadenas.add("juan");
        cadenas.add("pepito");
        cadenas.add("carlos");



        LinkedList<String> lista1 = new LinkedList<String>();

        for (int i = 0; i < cadenas.size(); i++){
            lista1.add(i, cadenas.get(i));
        }
        System.out.println("Imprimimos la ArrayList original " + cadenas.toString());
        System.out.println("Imprimimos la linkedList " + lista1.toString());

    }


}
