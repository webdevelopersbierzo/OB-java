package apartado4;

import java.util.Vector;

/*
 * Crea un vecto del tipo de dato que prefieras, y añadele 5 elementos, Elimina el 2º y 3º elemento y muestra
 * el resultado final
 */
public class Apartado4 {
    public static void main(String[] args) {
        Vector<Integer> numeros = new Vector<>();
        numeros.add(10);
        numeros.add(20);
        numeros.add(30);
        numeros.add(40);
        numeros.add(50);
        System.out.println("El vector inicial es :"+ numeros);

        // removemos 2º y 3º elemento

        numeros.remove(1); // segunda posicion
        System.out.println("despues de remover el segundo elemento"+ numeros);
        numeros.remove(1); // eliminamos el segundo elemento que corresponde al tercero del vector original

        System.out.println(" El vector despues de remover el 2 y 3 elemento es " + numeros);

    }
}